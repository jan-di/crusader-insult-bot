#!/bin/sh

wget -O /tmp/phive.phar "https://phar.io/releases/phive.phar"
wget -O /tmp/phive.phar.asc "https://phar.io/releases/phive.phar.asc"
gpg --keyserver hkps.pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
gpg --verify /tmp/phive.phar.asc /tmp/phive.phar
rm /tmp/phive.phar.asc
chmod +x /tmp/phive.phar
mv /tmp/phive.phar /usr/local/bin/phive