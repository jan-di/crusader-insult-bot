<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserInsultRepository")
 * @ORM\Table(name="user_insult")
 */
class UserInsult
{
    /**
     * @ORM\Id
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userInsults")
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\JoinColumn(name="insult_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Insult", inversedBy="userInsults")
     */
    private $insult;

    /**
     * @ORM\Column(name="count", type="integer")
     */
    private $count;

    public function __construct(User $user, Insult $insult)
    {
        $this->setUser($user)
            ->setInsult($insult);
    }

    public function getUser(): User
    {
        return $this->user;
    }

    private function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getInsult(): Insult
    {
        return $this->insult;
    }

    private function setInsult(Insult $insult): self
    {
        $this->insult = $insult;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function incrementCount(): self
    {
        ++$this->count;

        return $this;
    }
}
