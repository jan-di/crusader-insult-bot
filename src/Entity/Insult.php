<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="insult")
 */
class Insult
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;

    /**
     * @ORM\Column(name="filename", type="string",length=30)
     */
    private $filename;

    /**
     * @ORM\OneToMany(targetEntity="UserInsult", mappedBy="insult")
     */
    private $userInsults;

    public function __construct(int $id, string $title, string $filename)
    {
        $this->userInsults = new ArrayCollection();

        $this->setId($id)
            ->setTitle($title)
            ->setFilename($filename);
    }

    public function getId(): int
    {
        return $this->id;
    }

    private function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getUserInsults(): iterable
    {
        return $this->userInsults;
    }
}
