<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=75)
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=75, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(name="username", type="string", length=50, nullable=true))
     */
    private $username;

    /**
     * @ORM\Column(name="language_code", type="string", length=35, nullable=true))
     */
    private $languageCode;

    /**
     * @ORM\OneToMany(targetEntity="UserInsult", mappedBy="user")
     */
    private $userInsults;

    public function __construct(int $id, string $firstName, ?string $lastName, ?string $username, ?string $languageCode)
    {
        $this->userInsults = new ArrayCollection();

        $this->setId($id)
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setUsername($username)
            ->setLanguageCode($languageCode);
    }

    public function getId(): int
    {
        return $this->id;
    }

    private function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?string $languageCode): self
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    public function getUserInsults(): iterable
    {
        return $this->userInsults;
    }
}
