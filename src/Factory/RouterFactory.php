<?php

declare(strict_types=1);

namespace App\Factory;

use App\Controller\HookController;
use App\Controller\IndexController;
use App\Controller\StatsController;
use League\Route\Router;
use League\Route\Strategy\ApplicationStrategy;
use Psr\Container\ContainerInterface;

class RouterFactory
{
    public static function createRouter(ContainerInterface $container): Router
    {
        $strategy = new ApplicationStrategy();
        $strategy->setContainer($container);
        $router = (new Router())
            ->setStrategy($strategy);

        self::injectRoutes($router, $container);

        return $router;
    }

    private static function injectRoutes(Router $router, ContainerInterface $container): void
    {
        $router->map('GET', '/', [IndexController::class, 'showIndex']);
        $router->map('GET', '/stats', [StatsController::class, 'showStats']);
        $router->map('POST', '/hook/{token:alphanum_dash}', [HookController::class, 'processUpdate']);
    }
}
