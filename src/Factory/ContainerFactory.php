<?php

declare(strict_types=1);

namespace App\Factory;

use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Psr\Container\ContainerInterface;

class ContainerFactory
{
    public static function createContainer(): ContainerInterface
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(true);
        $containerBuilder->useAnnotations(false);
        $containerBuilder->addDefinitions(
            ...glob(__DIR__.'/../../config/definitions/*.php')
        );

        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();

        return $containerBuilder->build();
    }
}
