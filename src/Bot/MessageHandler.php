<?php

declare(strict_types=1);

namespace App\Bot;

use App\Entity\User;
use Psr\Http\Message\RequestInterface;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Objects\BaseObject;

class MessageHandler extends AbstractUpdateHandler
{
    public function process(User $user, BaseObject $message, RequestInterface $request): void
    {
        $keyboard = Keyboard::make()->inline()
        ->row(
            Keyboard::inlineButton(['text' => 'choose chat', 'switch_inline_query' => ''])
        );

        $this->bot->sendMessage([
            'chat_id' => $message->chat->id,
            'text' => 'Please use me inline :)',
            'reply_markup' => $keyboard,
        ]);
    }
}
