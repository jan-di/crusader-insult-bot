<?php

declare(strict_types=1);

namespace App\Bot;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\RequestInterface;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\BaseObject;

abstract class AbstractUpdateHandler
{
    protected $bot;
    protected $entityManager;

    public function __construct(Api $bot, EntityManager $entityManager)
    {
        $this->bot = $bot;
        $this->entityManager = $entityManager;
    }

    abstract public function process(User $user, BaseObject $object, RequestInterface $request): void;
}
