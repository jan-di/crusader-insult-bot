<?php

declare(strict_types=1);

namespace App\Bot;

use App\Entity\Insult;
use App\Entity\User;
use App\Entity\UserInsult;
use Psr\Http\Message\RequestInterface;
use Telegram\Bot\Objects\BaseObject;

class ChosenInlineResultHandler extends AbstractUpdateHandler
{
    public function process(User $user, BaseObject $inlineResult, RequestInterface $request): void
    {
        $insultId = intval($inlineResult->resultId);
        $userInsultRepository = $this->entityManager->getRepository(UserInsult::class);
        $userInsult = $userInsultRepository->findById($user->getId(), $insultId);
        if (!$userInsult) {
            $insultRepository = $this->entityManager->getRepository(Insult::class);
            $insult = $insultRepository->find($insultId);
            $userInsult = new UserInsult($user, $insult);
            $this->entityManager->persist($userInsult);
        }
        $userInsult->incrementCount();
    }
}
