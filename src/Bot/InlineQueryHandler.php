<?php

declare(strict_types=1);

namespace App\Bot;

use App\Entity\Insult;
use App\Entity\User;
use Psr\Http\Message\RequestInterface;
use Telegram\Bot\Objects\BaseObject;
use Telegram\Bot\Objects\InlineQuery\InlineQueryResultVoice;

class InlineQueryHandler extends AbstractUpdateHandler
{
    public function process(User $user, BaseObject $inlineQuery, RequestInterface $request): void
    {
        $baseUri = $request->getUri()->withPath('')->withQuery('');
        $insultRepository = $this->entityManager->getRepository(Insult::class);
        $insults = $insultRepository->findAll();
        $inlineQueryResults = array_map(function (Insult $insult) use ($baseUri) {
            $result = new InlineQueryResultVoice();
            $result->setId($insult->getId());
            $result->setTitle($insult->getTitle());
            $result->setVoiceUrl((string) $baseUri->withPath('/insults/'.$insult->getFilename()));

            return $result;
        }, $insults);

        $this->bot->answerInlineQuery([
            'inline_query_id' => $inlineQuery->id,
            'results' => json_encode($inlineQueryResults),
        ]);
    }
}
