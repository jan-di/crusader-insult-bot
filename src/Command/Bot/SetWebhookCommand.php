<?php

declare(strict_types=1);

namespace App\Command\Bot;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class SetWebhookCommand extends Command
{
    protected static $defaultName = 'bot:set-webhook';
    private $bot;

    public function __construct(Api $bot)
    {
        $this->bot = $bot;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Set the webhook url');

        $this->addArgument('url', InputArgument::REQUIRED, 'Webhook Url');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->bot->setWebhook([
                'url' => $input->getArgument('url'),
            ]);
        } catch (TelegramSDKException $e) {
            $io->error("Could not set webhook:\n".$e->getMessage());

            return 1;
        }

        $io->success('Changed webhook url to '.$input->getArgument('url'));

        return 0;
    }
}
