<?php

declare(strict_types=1);

namespace App\Command\Bot;

use DateTimeImmutable;
use DateTimeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Telegram\Bot\Api;

class GetWebhookCommand extends Command
{
    protected static $defaultName = 'bot:get-webhook';
    private $bot;

    public function __construct(Api $bot)
    {
        $this->bot = $bot;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Get information about the configured webhook');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $info = $this->bot->getWebhookInfo();

        $io->table(
            ['Key', 'Value'],
            [
                ['Url', $info->get('url')],
                ['Custom Certificate', $info->get('has_custom_certificate') ? 'yes' : 'no'],
                ['Max Connections', $info->get('max_connections')],
                ['Allowed Updates', $info->get('allowed_updates') ?? '*'],
                ['Pending Updates', $info->get('pending_update_count')],
                ['Last Error Time', $info->get('last_error_date') ? DateTimeImmutable::createFromFormat('U', (string) $info->get('last_error_date'))->format(DateTimeInterface::ATOM) : ''],
                ['Last Error Message', $info->get('last_error_message')],
            ]
        );

        return 0;
    }
}
