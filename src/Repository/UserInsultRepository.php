<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class UserInsultRepository extends EntityRepository
{
    public function findById(int $userId, int $insultId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT ui FROM App\Entity\UserInsult ui JOIN ui.user u JOIN ui.insult i WHERE u.id = :userId AND i.id = :insultId'
            )
            ->setParameters([
                'userId' => $userId,
                'insultId' => $insultId,
            ])
            ->getResult()[0] ?? null;
    }
}
