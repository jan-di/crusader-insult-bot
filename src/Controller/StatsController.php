<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Laminas\Diactoros\Response\HtmlResponse;
use PDO;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class StatsController
{
    private $entityManager;
    private $twig;

    public function __construct(
        EntityManager $entityManager,
        Environment $twig
    ) {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    public function showStats(): ResponseInterface
    {
        
        $sql = " 
            SELECT id, title, COALESCE(SUM(count), 0) AS count FROM insult
            LEFT JOIN user_insult ON insult.id = user_insult.insult_id
            GROUP BY insult.id
            ORDER BY count DESC, id
        ";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $insults = $stmt->fetchAll(PDO::FETCH_OBJ);

        $categories = [];
        $values = [];
        foreach ($insults as $insult) {
            $categories[] = $insult->title;
            $values[] = intval($insult->count) / intval($insults[0]->count);
            // $values[] = intval($insult->count);
        }


        $resp = new HtmlResponse($this->twig->render('stats.html.twig', [
            'categoryJson' => json_encode($categories),
            'valuesJson' => json_encode($values)
        ]));

        return $resp;
    }
}
