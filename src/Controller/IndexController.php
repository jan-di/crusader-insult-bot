<?php

declare(strict_types=1);

namespace App\Controller;

use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ResponseInterface;

class IndexController
{
    public function showIndex(): ResponseInterface
    {
        return new TextResponse('');
    }
}
