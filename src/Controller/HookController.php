<?php

declare(strict_types=1);

namespace App\Controller;

use App\Bot\ChosenInlineResultHandler;
use App\Bot\InlineQueryHandler;
use App\Bot\MessageHandler;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Telegram\Bot\Api;

class HookController
{
    private $bot;
    private $responseFactory;
    private $entityManager;
    private $messageHandler;
    private $inlineQueryHandler;
    private $chosenInlineResultHandler;

    public function __construct(
        Api $bot,
        ResponseFactoryInterface $responseFactory,
        EntityManager $entityManager,
        MessageHandler $messageHandler,
        InlineQueryHandler $inlineQueryHandler,
        ChosenInlineResultHandler $chosenInlineResultHandler
    ) {
        $this->bot = $bot;
        $this->responseFactory = $responseFactory;
        $this->entityManager = $entityManager;
        $this->messageHandler = $messageHandler;
        $this->inlineQueryHandler = $inlineQueryHandler;
        $this->chosenInlineResultHandler = $chosenInlineResultHandler;
    }

    public function processUpdate(ServerRequestInterface $request, array $args): ResponseInterface
    {
        if ($args['token'] !== getenv('HOOK_TOKEN')) {
            return $this->responseFactory->createResponse(404);
        }

        $update = $this->bot->getWebhookUpdates();
        $telegramUser = $update->getMessage()->from;

        $userRepository = $this->entityManager->getRepository(User::class);

        $user = $userRepository->find($telegramUser->id);
        if ($user) {
            $user->setFirstName($telegramUser->firstName)
                ->setLastName($telegramUser->lastName)
                ->setUsername($telegramUser->username)
                ->setLanguageCode($telegramUser->languageCode);
        } else {
            $user = new User(
                $telegramUser->id,
                $telegramUser->firstName,
                $telegramUser->lastName,
                $telegramUser->username,
                $telegramUser->languageCode
            );
            $this->entityManager->persist($user);
        }

        switch ($update->detectType()) {
            case 'inline_query':
                $this->inlineQueryHandler->process($user, $update->inlineQuery, $request);
                break;
            case 'chosen_inline_result':
                $this->chosenInlineResultHandler->process($user, $update->chosenInlineResult, $request);
                break;
            case 'message':
                $this->messageHandler->process($user, $update->getMessage(), $request);
                break;
        }

        $this->entityManager->flush();

        return $this->responseFactory->createResponse(200);
    }
}
