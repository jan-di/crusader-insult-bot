<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200131200710 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE insult (id INT NOT NULL, title VARCHAR(50) NOT NULL, filename VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT NOT NULL, first_name VARCHAR(75) NOT NULL, last_name VARCHAR(75) DEFAULT NULL, username VARCHAR(50) DEFAULT NULL, language_code VARCHAR(35) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_insult (user_id INT NOT NULL, insult_id INT NOT NULL, count INT NOT NULL, INDEX IDX_68F91967A76ED395 (user_id), INDEX IDX_68F91967745876B9 (insult_id), PRIMARY KEY(user_id, insult_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_insult ADD CONSTRAINT FK_68F91967A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_insult ADD CONSTRAINT FK_68F91967745876B9 FOREIGN KEY (insult_id) REFERENCES insult (id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_insult DROP FOREIGN KEY FK_68F91967745876B9');
        $this->addSql('ALTER TABLE user_insult DROP FOREIGN KEY FK_68F91967A76ED395');
        $this->addSql('DROP TABLE insult');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_insult');
    }
}
