<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200131203748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO insult (id, title, filename) VALUES
            (1, "Esel!", "insult01.ogg"),
            (2, "Versager!", "insult02.ogg"),
            (3, "Schwachsinniger Narr!", "insult03.ogg"),
            (4, "Klappe, Knappe!", "insult04.ogg"),
            (5, "Du Bauer!", "insult05.ogg"),
            (6, "Hilfe, Hilfe!", "insult06.ogg"),
            (7, "Dieses Mal werde ich Euch besiegen!", "insult07.ogg"),
            (8, "Ich lass Euch hängen!", "insult08.ogg"),
            (9, "Verdammter Narr!", "insult09.ogg"),
            (10, "Whhrraaaa!", "insult10.ogg"),
            (11, "Flegel!", "insult11.ogg"),
            (12, "Hanswurst!", "insult12.ogg"),
            (13, "Fort mit dir, Pursche!", "insult13.ogg"),
            (14, "Fatzke!", "insult14.ogg"),
            (15, "Memme!", "insult15.ogg"),
            (16, "Tölpel!", "insult16.ogg"),
            (17, "Lackaffe!", "insult17.ogg"),
            (18, "Etwas senil, was?", "insult18.ogg"),
            (19, "Hinterhältiges Wiesel", "insult19.ogg"),
            (20, "Laffe!", "insult20.ogg")
        ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM insult');
    }
}
