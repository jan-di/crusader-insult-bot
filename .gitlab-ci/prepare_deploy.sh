#!/bin/sh

set -e

apk --no-cache add \
    gettext \
    openssh \
    rsync

mkdir -p ~/.ssh
echo "$DEPLOY_SSH_FINGERPRINT" >> ~/.ssh/known_hosts
