#!/bin/sh

set -e

SSH_PREFIX="ssh -p$DEPLOY_SSH_PORT $DEPLOY_SSH_USERNAME@$DEPLOY_SSH_HOST"

eval $(ssh-agent -s)
cat "$DEPLOY_SSH_KEY" | tr -d '\r' | ssh-add - > /dev/null

envsubst < ".gitlab-ci/data/.env.template" > ".env.template"

$SSH_PREFIX "mkdir -p ${DEPLOY_DIR}_tmp"
rsync -av -e ssh --exclude='.git/' --filter=':- .gitignore' --delete-excluded ./ $DEPLOY_SSH_USERNAME@$DEPLOY_SSH_HOST:"${DEPLOY_DIR}_tmp"

$SSH_PREFIX "cd ${DEPLOY_DIR}_tmp && ${DEPLOY_COMPOSER_BIN} install --no-dev --no-suggest --no-progress --classmap-authoritative --prefer-dist"
$SSH_PREFIX "mv ${DEPLOY_DIR}_tmp/.env.template ${DEPLOY_DIR}_tmp/.env"
$SSH_PREFIX "${DEPLOY_PHP_BIN} ${DEPLOY_DIR}_tmp/bin/console orm:clear-cache:metadata"
$SSH_PREFIX "${DEPLOY_PHP_BIN} ${DEPLOY_DIR}_tmp/bin/console orm:generate-proxies"
$SSH_PREFIX "${DEPLOY_PHP_BIN} ${DEPLOY_DIR}_tmp/bin/console migrations:migrate"

$SSH_PREFIX "mv ${DEPLOY_DIR} ${DEPLOY_DIR}_old && mv ${DEPLOY_DIR}_tmp ${DEPLOY_DIR}"
$SSH_PREFIX "rm -rf ${DEPLOY_DIR}_old"
