<?php

declare(strict_types=1);

use App\Factory\ContainerFactory;
use App\Factory\RouterFactory;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

require_once dirname(__DIR__).'/vendor/autoload.php';

$container = ContainerFactory::createContainer();
$router = RouterFactory::createRouter($container);

$request = ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
$response = $router->dispatch($request);

$emitter = new SapiEmitter();
$emitter->emit($response);
