<?php

declare(strict_types=1);

use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelper;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelperInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;
use function DI\env;
use function DI\get;
use function DI\string as str;

return [
    'db.host' => env('DB_HOST', 'localhost'),
    'db.port' => env('DB_PORT', 3306),
    'db.name' => env('DB_NAME'),
    'db.username' => env('DB_USERNAME'),
    'db.password' => env('DB_PASSWORD'),
    'db.charset' => env('DB_CHARSET', 'utf8'),

    'orm.connection' => [
        'driver' => 'pdo_mysql',
        'host' => get('db.host'),
        'port' => get('db.port'),
        'dbname' => get('db.name'),
        'user' => get('db.username'),
        'password' => get('db.password'),
        'charset' => get('db.charset'),
    ],

    'orm.entity_dir' => str('{app.root}/src/Entity'),
    'orm.proxy_dir' => str('{app.root}/var/doctrine/proxy'),
    'orm.cache_dir' => str('{app.root}/var/doctrine/cache'),

    'orm.config' => function (ContainerInterface $c) {
        $isDevMode = $c->get('app.env') !== 'production';

        return Setup::createAnnotationMetadataConfiguration(
            [$c->get('orm.entity_dir')],
            $isDevMode,
            $c->get('orm.proxy_dir'),
            new FilesystemCache($c->get('orm.cache_dir')),
            false
        );
    },

    'migrations.config' => function (ContainerInterface $c, EntityManager $entityManager) {
        $configuration = new Configuration($entityManager->getConnection());
        $configuration->setMigrationsNamespace('App\Migrations');
        $configuration->setMigrationsTableName('database_migrations');
        $configuration->setMigrationsColumnName('database_version');
        $configuration->setMigrationsColumnLength(14);
        $configuration->setMigrationsExecutedAtColumnName('executed_at');
        $configuration->setMigrationsDirectory($c->get('app.root').'/src/Migration');

        return $configuration;
    },

    EntityManagerInterface::class => get(EntityManager::class),
    EntityManager::class => function (ContainerInterface $c) {
        return EntityManager::create($c->get('orm.connection'), $c->get('orm.config'));
    },

    ConnectionHelper::class => function (EntityManager $entityManager) {
        return new ConnectionHelper($entityManager->getConnection());
    },

    ConfigurationHelperInterface::class => get(ConfigurationHelper::class),
    ConfigurationHelper::class => function (ContainerInterface $c, EntityManager $entityManager) {
        return new ConfigurationHelper($entityManager->getConnection(), $c->get('migrations.config'));
    },
];
