<?php

declare(strict_types=1);

use Telegram\Bot\Api;
use function DI\autowire;
use function DI\env;
use function DI\get;

return [
    'hook.token' => env('HOOK_TOKEN'),
    'bot.token' => env('BOT_TOKEN'),

    Api::class => autowire(Api::class)
        ->constructor(get('bot.token')),
];
