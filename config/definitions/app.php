<?php

declare(strict_types=1);

use App\Command\Bot\GetWebhookCommand;
use App\Command\Bot\SetWebhookCommand;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelperInterface;
use Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand as ClearMetadataCacheCommand;
use Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use function DI\env;
use function DI\get;

return [
    'app.root' => realpath(__DIR__.'/../..'),
    'app.name' => 'CrusaderInsultBot',
    'app.env' => env('APP_ENV', 'production'),

    'helperset' => function (ContainerInterface $c) {
        return new HelperSet([
            'question' => $c->get(QuestionHelper::class),
            'db' => $c->get(ConnectionHelper::class),
            'em' => $c->get(EntityManagerHelper::class),
            'configuration_helper' => $c->get(ConfigurationHelperInterface::class),
        ]);
    },

    'commands' => [
        get(GetWebhookCommand::class),
        get(SetWebhookCommand::class),
        get(GenerateProxiesCommand::class),
        get(ClearMetadataCacheCommand::class),
    ],
];
