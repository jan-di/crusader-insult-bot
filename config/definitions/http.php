<?php

declare(strict_types=1);

use Buzz\Client\Curl;
use Laminas\Diactoros\RequestFactory;
use Laminas\Diactoros\ResponseFactory;
use Laminas\Diactoros\StreamFactory;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use function DI\create;
use function DI\get;

return [
    // PSR-17 Factories
    ResponseFactoryInterface::class => get(ResponseFactory::class),
    RequestFactoryInterface::class => get(RequestFactory::class),
    StreamFactoryInterface::class => get(StreamFactory::class),

    // PSR-18 HTTP Client
    ClientInterface::class => create(Curl::class)
        ->constructor(get(ResponseFactoryInterface::class)),
];
