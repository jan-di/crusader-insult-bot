<?php

declare(strict_types=1);

use Telegram\Bot\Api;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Loader\LoaderInterface;

use function DI\autowire;
use function DI\create;
use function DI\env;
use function DI\get;
use function DI\string as str;

return [
    'twig.template_dir' => str('{app.root}/template'),

    LoaderInterface::class => get(FilesystemLoader::class),
    FilesystemLoader::class => create(FilesystemLoader::class)
        ->constructor(get('twig.template_dir'))
];
